# Copyleft (c) March, 2022, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune pamac" \
  description="Dune in pamac" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-pamac/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-pamac" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG AUR_LIBPAMAC="https://aur.archlinux.org/cgit/aur.git/snapshot/libpamac-aur.tar.gz"

ARG AUR_PAMAC="https://aur.archlinux.org/cgit/aur.git/snapshot/pamac-aur.tar.gz"

RUN sudo pacman -Syyuq --needed --noconfirm && \
  mkdir -p ~/build && \
  cd ~/build && \
  curl -LO ${AUR_LIBPAMAC} && \
  tar -xvf libpamac-aur.tar.gz && \
  pushd libpamac-aur && \
  makepkg --noconfirm -si && \
  popd && \
  curl -LO ${AUR_PAMAC} && \
  tar -xvf pamac-aur.tar.gz && \
  pushd pamac-aur && \
  makepkg --noconfirm -si && \
  popd && \
  rm -rf ~/build ~/.cache /tmp/makepkg && \
  pacman -Qtdq | xargs -r sudo pacman --noconfirm -Rcns && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*